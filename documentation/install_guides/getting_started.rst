.. _getting started:

Getting started
===============

Accessing the Web UI
--------------------

|oA| can be managed using a web-based user interface, if the package
``openattic-gui`` has been installed and the Apache http Server has been
restarted afterwards.

Open a web browser and navigate to http://openattic.yourdomain.com/openattic

The default login username is **openattic**. Use the password you defined during
the :ref:`post-installation configuration`.

See the :ref:`gui_docs_index` for further information.
