'use strict';

var wizardCommon = function(){
  var self = this;
  var helpers = require('../../common.js');

  this.wizardOverviewBtn = element(by.className('tc_wizardOverview'));
  this.previousBtn = element(by.className('tc_previousBtn'));
  this.nextBtn = element(by.id('nextBtn'));
  this.volumefield = element(by.model('result.name'));
  this.size = element(by.model('data.megs'));
  this.volume_required = element(by.className('tc_nameRequired'));
  this.volume_unique = element.all(by.className('tc_noUniqueName')).get(0);
  this.pool_required = element(by.className('tc_poolRequired'));
  this.size_required = element(by.className('tc_sizeRequired'));
  this.size_exceeded = element(by.className('tc_wrongVolumeSize'));
  this.noValidNumber = element(by.className('tc_noValidNumber'));
  this.cifsName = element(by.id('cifsname'));
  this.configs = require('../../configs.js');

  this.openWizard = function(wizardName){
    browser.sleep(helpers.configs.sleep);
    element.all(by.className('tc_wizardTitle')).filter(function(wizard){
      return wizard.getText().then(function(text){
        return text === wizardName;
      });
    }).first().click();
  };

  this.creationPageElementCheck = function(pageTitle){
    //Step 1 - Create Volume
    //check available buttons
    expect(self.wizardOverviewBtn.isDisplayed()).toBe(true);
    expect(self.previousBtn.isDisplayed()).toBe(true);

    //check if angular expression contains 'Next' or 'Done
    expect(self.nextBtn.getText()).toEqual('Next');
    //check content of first wizard site
    expect(element(by.css('.tc_oawizard_h3')).getText()).toEqual(pageTitle);
    expect(self.volumefield.isDisplayed()).toBe(true);
    //expect(pool.isDisplayed()).toBe(true);
    expect(self.size.isDisplayed()).toBe(true);
    //expect(is_protected.isDisplayed()).toBe(true);
  };

  this.creationPagePoolSelection = function(poolType){
    //in order to enter a size we need to choose a pool first
    for(var key in self.configs.pools){
      var volumePoolSelect = element(by.model('pool'));
      var pool = self.configs.pools[key];
      volumePoolSelect.click();
      element.all(by.cssContainingText('option', '(' + poolType + ',')).get(0).click();
      //browser.actions().sendKeys( protractor.Key.ENTER ).perform();
      break;
    }
  };

  this.creationPageInputTests = function(){
    self.size.sendKeys('asdffffweee');
    expect(self.noValidNumber.isDisplayed()).toBe(true);
    self.size.clear();
    self.size.sendKeys('10000000000000000000000000000000');
    expect(self.size_exceeded.isDisplayed()).toBe(true);
  };

  this.creationPageValidationTests = function(){
    //check what happens if next button has been clicked without entering any data
    self.nextBtn.click();
    expect(self.volume_required.isDisplayed()).toBe(true);
    expect(self.pool_required.isDisplayed()).toBe(true);
    expect(self.size_required.isDisplayed()).toBe(true);

    //enter some data for validation
    self.volumefield.sendKeys('äasdower dsafodf');
    var noValidName = element(by.css('.tc_noValidName')).evaluate('errortext');
    expect(noValidName.isDisplayed()).toBe(true);

    //in order to enter a size we need to choose a pool first
    for(var key in self.configs.pools){
      var volumePoolSelect = element(by.model('pool'));
      var pool = self.configs.pools[key];
      volumePoolSelect.click();
      element.all(by.cssContainingText('option', '(volume group,')).get(0).click();
      //browser.actions().sendKeys( protractor.Key.ENTER ).perform();
      break;
    }
  };

  this.creationFromFill = function(volName, size, fsType){
    //enter some data to get to the next site
    self.volumefield.clear();
    self.volumefield.sendKeys(volName);
    self.size.clear();
    self.size.sendKeys(size);
    if(fsType){
      element(by.id(fsType)).click();
    }
    expect(self.volume_unique.isDisplayed()).toBe(false);
    self.nextBtn.click();
  };

  this.shareCreationElementCheck = function(pageTitle){
    //Step 2 - create share

    expect(element(by.css('.tc_step2')).getText()).toEqual(pageTitle);

    expect(self.wizardOverviewBtn.isDisplayed()).toBe(true);
    expect(self.previousBtn.isDisplayed()).toBe(true);
    expect(self.nextBtn.getText()).toEqual('Next');
  };

  this.shareCreateNfs = function(shareName){
    expect(element(by.model('input.cifs.create')).isPresent()).toBe(true);
    expect(element(by.model('input.nfs.create')).isPresent()).toBe(true);
    //choose nfs
    element(by.model('input.nfs.create')).click();
    var address = element(by.id('nfsaddress'));
    var options = element(by.id('nfsoptions'));

    browser.sleep(helpers.configs.sleep);
    expect(address.isPresent()).toBe(true);
    expect(element(by.id('nfsoptions')).isDisplayed()).toBe(true);
    expect(options.getAttribute('value')).toEqual('rw,no_subtree_check,no_root_squash');
    address.clear();
    self.nextBtn.click();
    expect(element(by.css('.tc_nfsAddressRequired')).isDisplayed()).toBe(true);
    address.sendKeys(shareName);
  };

  this.shareCreateCifs = function(volumeName, shareName){
    expect(element(by.model('input.cifs.create')).isPresent()).toBe(true);
    expect(element(by.model('input.nfs.create')).isPresent()).toBe(true);
    element(by.model('input.cifs.create')).click();

    //CIFS SHARE
    expect(self.cifsName.isPresent()).toBe(true);
    expect(self.cifsName.getAttribute('value')).toEqual(volumeName);

    expect(element(by.id('cifscomment')).isDisplayed()).toBe(true);

    self.cifsName.clear();
    self.nextBtn.click();

    expect(element(by.css('.tc_cifsNameRequired')).isDisplayed()).toBe(true);

    self.cifsName.sendKeys(shareName);
  };

  this.shareCreateFc = function(hostname, iscsi){
    if(!iscsi){
      //select host
      var hostSelect = element(by.model('input.iscsi_fc.host'));
      hostSelect.element(by.cssContainingText('option', hostname)).click();
    }else{
      element(by.className('tc_create_host')).click();
      element(by.model('host.name')).sendKeys(hostname);
      element.all(by.model('type.check')).get(0).click();
      element.all(by.model('data[key]')).get(0).click();
      element.all(by.model('newTag.text')).get(0).sendKeys(iscsi);
    }
  };

  this.configurationExecution = function(pageTitle){
    //Step 3 - Done
    browser.sleep(helpers.configs.sleep);
    expect(element(by.css('.tc_wizardDone')).getText()).toEqual(pageTitle);
    expect(self.nextBtn.getText()).toEqual('Done');
    self.nextBtn.click();
    browser.sleep(helpers.configs.sleep);
    expect(browser.getCurrentUrl()).toContain('/openattic/#');
    self.checkWizardTitles();
  };

  this.checkWizardTitles = function(){
    var wizards = element.all(by.repeater('wizard in wizards'))
      .then(function(wizards){
        wizards[0].element(by.css('.tc_wizardTitle')).evaluate('wizard.title').then(function(title){
          expect(title).toEqual('File Storage');
          //console.log(title);
        });

        wizards[1].element(by.css('.tc_wizardTitle')).evaluate('wizard.title').then(function(vm_title){
          expect(vm_title).toEqual('VM Storage');
          //console.log(vm_title);
        });

        wizards[2].element(by.css('.tc_wizardTitle')).evaluate('wizard.title').then(function(block_title){
          expect(block_title).toEqual('iSCSI/Fibre Channel target');
          //console.log(block_title);
        });
      });
  };

  this.handleFirstPage = function(pageTitle, poolType, volName, size, fsType){
    self.creationPageElementCheck(pageTitle);
    self.creationPageValidationTests();
    self.creationPagePoolSelection(poolType);
    self.creationPageInputTests();
    self.creationFromFill(volName, size, fsType);
  };
};

module.exports = wizardCommon;
