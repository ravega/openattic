��    4      �  G   \      x      y  &   �  ,   �     �     �       K   %  <   q  ;   �  6   �  3   !      U  &   v     �     �     �     �  	   �     �  
   �     �               !     2     A     R     Y     b     �     �     �     �     �     �     �     �     �     �     �  	   �     �               3  	   8     B     Q     ]     e     m  y  t  :   �	  6   )
  :   `
     �
  "   �
     �
  S   �
  E   0  A   v  =   �  P   �  :   G  6   �     �     �     �     �  
   �     �  
     
     	   $     .     C     X     q     �  
   �  (   �     �     �     �     �                    #     2     9     J     O     a  ,   j      �     �  	   �     �     �  	   �     �     �         "      +   .       2   !                ,          -                   3   )                                            (      0              	   4               *          %   
   #       '   /   1           &                $                      LV names may not be '.' or '..'. LV names must not begin with a hyphen. LVM does not support snapshotting snapshots. RAID RAID without initial sync RAID/thin pool metadata The following characters are valid for VG and LV names: a-z A-Z 0-9 + _ . - The vg field is required unless you are creating a snapshot. The volume name must not begin with 'snapshot' or 'pvmove'. The volume name must not contain '_mlog' or '_mimage'. This name clashes with a file or directory in /dev. VG names may not be '.' or '..'. VG names must not begin with a hyphen. active anywhere anywhere allocation cling allocation clustered contiguous allocation conversion exported image inherited allocation invalid snapshot merging origin merging snapshot mirror mirrored mirrored without initial sync non-resizable normal allocation offline online open origin out-of-sync image partial pvmove pvmove in progress raid read-only snapshot snapshot merge failed temporarily read-only thin thin pool thin pool data thin volume unknown virtual zeroed Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-03-09 15:20+0100
PO-Revision-Date: 2014-02-25 11:24
Last-Translator:   <>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1)
X-Translated-Using: django-rosetta 0.6.6
 Als LV-Namen dürfen '.' oder '..' nicht verwendet werden. LV-Namen dürfen nicht mit einem Bindestrich beginnen. LVM unterstützt nicht, Snapshots von Snapshots anzulegen. RAID RAID ohne initiale Synchronisation RAID/Thin Pool Metadaten Die folgenden Zeichen sind für die VG- und LV-Namen zulässig: a-z A-Z 0-9 + _ . - Das VG-Feld wird benötigt, es sei denn Sie erstellen einen Snapshot. Der Volume-Name darf nicht mit 'snapshot' oder 'pvmove' beginnen. Der Volume-Name darf '_mlog' oder '_mimage' nicht beinhalten. Dieser Name steht in Konflikt mit einer Datei oder einem Verzeichnis unter /dev. Als VG-Namen dürfen '.' oder '..' nicht verwendet werden. VG-Namen dürfen nicht mit einem Bindestrich beginnen. Aktiv Irgendwo Anywhere Allocation Cling Allocation Geclustert Contiguous Allocation Umwandlung Exportiert Abbildung Inherited Allocation Ungültiger Snapshot Ursprung zusammenführen Snapshot zusammenführen Spiegel Gespiegelt Ohne initiale Synchronisation gespiegelt Nicht größenveränderbar Normal Allocation Offline online Offen Ursprung out-of-sync image Unvollständig Pvmove Pvmove in Arbeit Raid Schreibgeschützt Snapshot Zusammenführen des Snapshots fehlgeschlagen Vorübergehend schreibgeschützt Thin Thin Pool Thin Pool Daten Thin Volume Unbekannt virtuell Genullt 