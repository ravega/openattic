��    Y      �     �      �     �     �     �  %   �     �  �   �     �     �     �     �     �     �     �     �     �     	     	     !	     (	  &   -	     T	     ]	     f	     s	     �	     �	     �	     �	     �	     �	     �	     �	  u   �	  	   D
     N
     \
     d
     i
     n
     }
     �
     �
  	   �
     �
     �
     �
  $   �
     �
     �
     �
           	       /        I  %   `  
   �     �  	   �     �     �     �     �     �     �          "  	   A  	   K  	   U  	   _     i     r  
   {  
   �     �  :   �  $   �  4   �  �   -  -   �     �     �     �     �  	               �  !     �     �     �  $   �       �   "     �     �     �          $     -  	   1     ;     B     K     b     o     v  $   }     �     �     �     �     �     �                    )     1  	   8  �   B  	   �     �     �     �     �                      	   &     0     7     F  $   V     {     �     �     �     �     �  Q   �       (   .  
   W     b  
   u  	   �  	   �     �  	   �     �     �  ,   �  ,     
   9  
   D     O  	   `     j     s     |     �     �  <   �  5   �  :     �   K  0   �  
   ,     7     @     G     O     X  
   a     K                  &   ,          +   C              8             A       0   D       V              L   N   5                /   O   X   <                  Y   J   7   
   2      *   S   H      =   #      	   P       F   >   '       :         Q                      W   B                               3       R   %            6              @       T      -   G       !   M           I   9   .       "       $   ?   (   E          4           1       )          ;      U    Additional Drives April August Automatically delete snapahots after: Available Items Before and after the snapshot is made, user-defined scripts can be executed. Please enter the path to your scripts or leave these fields empty if you don't have any. Clear Collapse all Configuration Assistant Create Schedule Created Day Day of Week Day(s) December Delete config Description Device Done Drag and drop additional volumes here: End date End time Execute date Execute immediately Execute later Execute time FS Type Feburary Finish Friday Hour Hour(s) If other devices need to be included in the snapshot (e.g. for raw device mappings), they can be added via Drag&Drop. Is active Item settings January July June Last execution March May Minute Minute(s) Monday Mount Point Mount Points Never automatically delete snapshots New configuration Next No end date November October Options Please define the retention time for snapshots. Please select the time Please select the week day and months Postscript Pre-/Post-Scripts Prescript Previous Reload Saturday Save Schedule Part 1 / Expiry Date Schedule Part 2 / Options Schedule Part 3 / Times Part 2 Schedule Part 3 / Times Part 3 Schedules Second(s) Select... September SnapApps Snapshot Start date Start time Sunday The configuration-name must be at least 5 characters long. The datetime must not be in the past The enddatetime must not be before the startdatetime The following wizard configures automated snapshots using the openATTIC SnapApps.<br /><br />Please enter a name for the new configuration. There are no configuration options available. Thursday Tuesday Volume Volumes Wednesday Week(s) Welcome Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-03-09 15:30+0100
PO-Revision-Date: 2014-03-09 15:30
Last-Translator: Michael Ziegler <michael.ziegler@it-novum.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: 
Plural-Forms: nplurals=2; plural=(n != 1)
X-Translated-Using: django-rosetta 0.6.6
 Zusätzliche Laufwerke April August Snapshots automatisch löschen nach: Verfügbare Objekte Bei der automatischen Ausführung der SnapApp-Konfiguration können benutzerdefinierte Skripte ausgeführt werden. <br /> Bitte geben Sie den Pfad zu Ihren Skripten an, sofern Sie welche haben. Leeren Alle einklappen Konfigurationsassistent Zeitplan anlegen Angelegt Tag Wochentag Tag(e) Dezember Konfiguration löschen Beschreibung Gerät Fertig Zusätzliche Volumes hierher ziehen: Enddatum Endzeit Datum der Ausführung Sofort ausführen Später ausführen Zeit der Ausführung Dateisystemtyp Februar Fertig stellen Freitag Stunde Stunde(n) Sollte ein zeitgleicher Snapshot von weiteren Volumes notwendig sein (z.B. Raw-Device-Mappings) können diese per Drag&Drop hinzugefügt werden. Ist aktiv Objekteinstellungen Januar Juli Juni Letzte Ausführung März Mai Minute Minute(n) Montag Einhängepunkt Einhängepunkte Snapshots nicht automatisch löschen Neue Konfiguration Weiter Kein Ablaufdatum November Oktober Optionen Bitte wählen Sie den Aufbewahrungszeitraum für automatisch erstellte Snapshots. Bitte wählen Sie die Zeit. Bitte wählen Sie Wochentage und Monate. Postskript Prä-/Post-Skripte Präskript Vorherige Neu laden Samstag Speichern Zeitplan Teil 1 / Ablaufdatum Zeitplan Teil 2 / Optionen Ablaufplanung Teil 3 / Zeitmanagement Teil 2 Ablaufplanung Teil 3 / Zeitmanagement Teil 3 Zeitpläne Sekunde(n) Bitte wählen... September SnapApps Snapshot Anfangsdatum Anfangszeit Sonntag Der Konfigurationsname muss mindestens 5 Zeichen beinhalten. Der Zeitpunkt darf nicht in der Vergangenheit liegen. Der Endzeitpunkt darf nicht vor dem Startzeitpunkt liegen. Im folgenden Assistenten werden automatisierte Snapshots auf Basis der openATTIC SnapApps konfiguriert. <br /><br /> Bitte wählen Sie einen Namen für die neue Konfiguration. Es sind keine Konfigurationsoptionen verfügbar! Donnerstag Dienstag Volume Volumes Mittwoch Woche(n) Willkommen 