��    	      d      �       �      �   	   �   G   �      ;     G     O  "   T  Z   w  �  �     v     ~  ]   �     �     �     �  @     x   C              	                                Address Directory If you wish to share only a subpath of the volume, enter the path here. Linux (NFS) Options Path Please select the volume to share. See <a href="http://man.cx/exports%285%29" target="_blank">the NFS manual</a> for details. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-06-12 13:26+0200
PO-Revision-Date: 2012-04-18 10:07
Last-Translator: Michael Ziegler <michael.ziegler@it-novum.com>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1)
X-Translated-Using: django-rosetta 0.6.6
 Adresse Verzeichnis Wenn Sie nur ein Verzeichnis des Volumes freigeben möchten, geben Sie bitte hier den Pfad an Linux (NFS) Optionen Pfad Bitte wählen Sie das Volume aus, welches Sie freigeben möchten Weitere Informationen finden Sie <a href="http://man.cx/exports%285%29/de" target="_blank">in der NFS-Dokumentation</a>. 