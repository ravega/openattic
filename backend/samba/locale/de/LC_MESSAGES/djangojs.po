# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-02-24 15:53+0100\n"
"PO-Revision-Date: 2014-02-25 09:17\n"
"Last-Translator:   <>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: \n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Translated-Using: django-rosetta 0.6.6\n"

#: templates/samba/share.js:21
msgid "Samba"
msgstr "Samba"

#: templates/samba/share.js:26 templates/samba/share.js.c:60
msgid "Share name"
msgstr "Freigabename"

#: templates/samba/share.js:30 templates/samba/share.js.c:66
msgid "Path"
msgstr "Pfad"

#: templates/samba/share.js:34 templates/samba/share.js.c:78
msgid "Available"
msgstr "Verfügbar"

#: templates/samba/share.js:72
msgid "Browseable"
msgstr "Sichtbar"

#: templates/samba/share.js:84
msgid "Writeable"
msgstr "Schreibbar"

#: templates/samba/share.js:90
msgid "Guest OK"
msgstr "Erlaube Gast"

#: templates/samba/share.js:95
msgid "Comment"
msgstr "Kommentar"

#: templates/samba/share.js:109
msgid "Windows (CIFS)"
msgstr "Windows (CIFS)"

#~ msgid "System user"
#~ msgstr "Systembenutzer"

#~ msgid "System Group"
#~ msgstr "Systemgruppe"

#~ msgid "Dir Mode"
#~ msgstr "Verzeichnis Berechtigungen"

#~ msgid "Set rights for the Directory"
#~ msgstr "Setze Rechte für Verzeichnis"

#~ msgid "Create Mode"
#~ msgstr "Berechtigungen"

#~ msgid "Set rights for owner, group and others"
#~ msgstr "Rechte für Besitzer, Gruppe und andere Personen"
