��    -      �      �      �  
   �     �               %     ,     2     ?     K     Y     h     {     �  *   �  
   �     �  
   �     �       �     
   �     �     �     �     �     �     �     �  	   �     �  
   �       <        H     M     S     Z     g     y     �  %   �     �     �  r   �  y  U     �     �     �       	     
   '     2     C     O     `     s     �     �  ?   �     �     �     	      	     4	  �   9	     �	     �	     �	  .   �	     (
     1
     6
     F
     T
     b
     j
     x
  >   
     �
  
   �
     �
     �
     �
  
   �
     �
  )        >     K  �   \   Add Volume Additional Settings As you wish. Block-based Cancel Close Collapse all Compression Create Volume Critical Level Critical Level (%) Deduplication Delete Volume Error! Needs to be less or equal maxvalue! Expand all File System File-based Hm, that doesn't seem right... Host If you want to use DRBD with this device, do not yet create a file system on it, even if you want to share it using NAS services later on. Loading... Mirror Name No config options available! Owner Path Querying data... Reload Select... Size Size in MB Status The volume group in which you want the Volume to be created. Type Used% Volume Volume Group Volume Management Volume Pool Volume Pool Management Waiting for volume group selection... Warning Level Warning Level (%) What was the name of the volume you wish to delete again?<br /><b>There is no undo and you will lose all data.</b> Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-02-24 15:56+0100
PO-Revision-Date: 2014-02-25 10:23
Last-Translator:   <>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: 
Plural-Forms: nplurals=2; plural=(n != 1)
X-Translated-Using: django-rosetta 0.6.6
 Volume hinzufügen Zusätzliche Einstellungen Wie sie wünschen. Blockbasiert Abbrechen Schließen Alles einklappen Kompression Volume erstellen Kritische Schwelle Kritische Schwelle (%) Deduplizierung Volume löschen Fehler! Der Wert muss kleiner oder gleich dem Maximalwert sein! Alles ausklappen Dateisystem Dateibasiert Hm, das sieht nicht richtig aus. Host Wenn Sie dieses Volume mithilfe von DRBD spiegeln möchten, erstellen Sie hier kein Dateisystem. Dies gilt auch, wenn Sie es später über NAS Dienste freigeben möchten. Lade... Spiegel Name Keine Konfigurationsmöglichkeiten verfügbar! Besitzer Pfad Datenabfrage... Aktualisieren Auswählen... Größe Größe in MB Status Die Volumegruppe, in der das neue Volume erstellt werden soll. Typ Verwendet% Volume Volumegruppe Volumeverwaltung Volumepool Volume Pool Verwaltung Warte auf die Auswahl der Volumegruppe... Warnschwelle Warnschwelle (%) Wie lautet noch einmal der Name des Volumes, das gelöscht werden soll?<br /><b>Diese Aktion kann nicht rückgängig gemacht werden. Sie verlieren all ihre Daten.</b> 